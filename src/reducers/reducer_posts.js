/**
 * Created by lokeshagrawal on 12/05/17.
 */

import { FETCH_POSTS, FETCH_POST, DELETE_POST } from '../actions/index';
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case FETCH_POSTS:
            console.log("Payload Inside Reducer:" + action.payload.data);

            // [post1, post2]
            // we have to transform payload to something like this { 4: post1 }
            return _.mapKeys(action.payload.data, 'id');
        case FETCH_POST:
            //ES5 Way
            //const post = action.payload.data;
            //const newState = { ...state };
            //newState[post.id] = post;
            //return newState;

            // ES6 way, here [] is a key interpolation
            console.log(action.payload.data);
            return { ...state, [action.payload.data.id]: action.payload.data };
        case DELETE_POST:
            // remove this 'action.payload' key from state and return a new object.
            return _.omit(state, action.payload);
        default:
            return state;
    }
}

// Brilliant method "mapKeys" from Lodash
/*
const posts = [
    {id:4, title:"Hi"},
    {id:3, title:"Hello"},
    {id:32, title:"Bye"},
];

const state = _.mapKeys(posts, 'id');

//coverts to this awesome thing over here
{
    "4":{"id":4, "titile":"Hi"},
    "3":{"id":3, "titile":"Hello"},
    "32":{"id":32, "titile":"Bye"}
}

console.log(state['4']);// {"id":4, "titile":"Hi"}
*/