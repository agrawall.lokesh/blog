import { combineReducers } from 'redux';
import PostReducer from './reducer_posts';

// as is an alias
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
    posts: PostReducer,
    form: formReducer
});

export default rootReducer;

// Internally redux-form uses our redux instance or our instance of the redux store
// for handling all of the state that has been produced by the form. Its helping us
// by saving us from writing bunch of action creators which form will need.

