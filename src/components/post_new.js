/**
 * Created by lokeshagrawal on 12/05/17.
 */
import React, {Component} from 'react';

// reduxForm is a function, very similar to connect helper that we used from react-redux
// redux-form allows us to communicate with that addition reducer which we wired inside index.js of reducers.
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { createPost } from '../actions/index';

class PostsNew extends Component {
    // field because whatever jsx we pass from here should be wired up with the field component.
    // this field object contains some event handlers that we to need wire up to the jsx that we will be returning.
    renderField(field) {

        // field.meta.error property is  automatically added to the field object from validate function.

        // fetch field.meta.touched, field.meta.error from field ES6 way
        // destructuring the values
        // const {meta: {touched, error}} = field;

        const className = `form-group ${field.meta.touched && field.meta.error ? 'has-danger': ''}`;
        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    className="form-control"
                    type="text"
                    // field.input is an object that contains bunch of different
                    // event handlers and bunch of different props, stuff like,
                    // onChange, onBlur, onFocus etc. It also has a value of the input.
                    // by doing ... in here we are saying, OK  field.input is an object right here  and I want
                    // all of the different propreties in this object to be communicated as
                    // props to the input tag. Fancy of JSX for something like
                    // onChange = {field.input.onChange}
                    // onFocus = {field.input.onFocus}
                    // onBlur = {field.input.onFocus}
                    {...field.input}
                />
                <div className="text-help">
                    {field.meta.touched ? field.meta.error : ''}
                </div>
            </div>
        )
    }

    onSubmit(values){
        // this === component
        console.log(values);

        // we have to pass a callback to the action creator, if action creator
        // calls this callback, it will automatically navigate us back to the list of post
        this.props.createPost(values, () => {
            this.props.history.push('/');
        });
    }

    render() {
        // name is for the state of that Field inside redux-form
        // Component property takes in a function, that will be used to display Field component.
        // label is a any arbitrary property name which will be attached to the field available inside renderField method.
        // we could have used name as well but label could be different a lot of times, hence have a separate label property.
        // field also have some automatically populated property.

        // handleSubmit is a redux side of the function, which after passing all validations
        // calls the function we defined that is this.onSubmit, onSubmit is a callback.
        //  handleSubmit is property that is passed to a component on behalf  of redux-form
        const {handleSubmit} = this.props;

        return (
            <div>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this)) }>
                    <Field
                        label="Title for Post"
                        name="title"
                        component={this.renderField}
                    />
                    <Field
                        label="Categories"
                        name="categories"
                        component={this.renderField}
                    />
                    <Field
                        label="Post Content"
                        name="content"
                        component={this.renderField}
                    />
                    <button type="submit" className="btn btn-primary">Submit</button>
                    <Link to="/" className="btn btn-danger"> Cancel </Link>
                </form>
            </div>
        );
    }
}

// write this function and pass it to the reduxForm below as a configuration.
// This function get called automatically at certain point during the forms lifecycle, actually when user tries to submit a form.
// values is an object that contains all the different values that user has entered into the form.
//
function validate(values){
    // check this console.log
    // console.log(values); ---> {title: '', categories: '', content: ''}
    const errors = {};

    /*validate the inputs from 'values'.
    if(values.title.length < 3) {
        errors.title = "Enter a title that is atleast 3 characters length!";
    }*/

    // if want both checks together
    if(!values.title  || values.title.length < 3 ) {
        errors.title = "Please enter a title that is atleast 3 characters!";
    }
    if(!values.categories) {
        errors.categories = "Enter some categories!";
    }
    if(!values.content) {
        errors.content = "Enter some content please!";
    }

    // if errors is empty, the form is fine to submit
    // if errors has *any* properties redux form assumes form is invalid.
    return errors;
}

// How to display these validation messages?
// It is our responsibility to show validation errors to user.

// wiring of reduxForm to the PostsNew component
// it needs second () input as a react component.
export default reduxForm(
    {
        validate: validate,
        form: 'PostsNewForm' // Name of the form, make sure the string 'PostsNewForm' is unique.
    })(
    connect(null, {createPost})(PostsNew)
);

/* export default reduxForm({
        validate: validate,
        form: 'PostsNewForm'
   })(PostsNew);
*/


/*
<Field
    name = "title"
    component = {this.renderTitleField}
/>

 <Field
 name = "categories"
 component = {this.renderCategoriesField}
 />

renderTitleField(field)
{
    return (
        <div className="form-group">
            <label> Title </label>
            <input
                className="form-control"
                type="text"
                {...field.input}
            />
            {field.meta.error}
        </div>

    )
}

// DRY (Dont repeat yourself), its same as that of renderTitleField
renderCategoriesField(field)
{
}
*/


