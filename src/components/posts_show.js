/**
 * Created by lokeshagrawal on 13/05/17.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchPost, deletePost} from '../actions/index';

// If user goes directly to this page we will fetch only this post.

class PostsShow extends Component {
    componentDidMount(){
        // if(!this.props.post) // if you dont want to refetch the post, if it exist in state.
        // I prefer to reload because content may have got changed.
        // provided by react-router
        const id = this.props.match.params.id;
        this.props.fetchPost(id);
    }

   onDeleteClick() {
        const id = this.props.match.params.id;
        this.props.deletePost(id, () => {
            // back to home page after successfully deletion of the post
            this.props.history.push('/');
        });
    }

    render(){
        if(!this.props.post) {
            return <div> Loading...</div>;
        }

        return(
            <div>
                <Link to="/"> Back to Index </Link>
                <button
                    className="btn btn-danger pull-xs-right"
                    onClick={this.onDeleteClick.bind(this)}
                >
                    Delete Post
                </button>
                <h3>{this.props.post.title}</h3>
                <h6>Categories: {this.props.post.categories}</h6>
                <p>{this.props.post.content}</p>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {post: state.posts[ownProps.match.params.id]}
}

export default connect(mapStateToProps, {fetchPost, deletePost})(PostsShow);

