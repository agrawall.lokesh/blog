import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import promise from 'redux-promise';

import reducers from './reducers';
import PostsIndex from './components/posts_index';
import PostsNew from './components/post_new';
import PostsShow from './components/posts_show';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
      <BrowserRouter>
          <div>
              <Switch>
                  <Route path="/posts/new" component={PostsNew}/>
                   /*:id wildcard. will be passed in as a property to PostsShow Component*/
                  <Route path="/posts/:id" component={PostsShow}/>
                  <Route path="/" component={PostsIndex}/>
              </Switch>
          </div>
      </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));


// Switch is used to overcome issue with routing.
// Switch components looks all of the routes components inside of it and it will decide to only render that matches the current URL.
// So remember to put most specific route above the list.
// Remember the order inside Switch, Most matched first.

// Simple Route Example to start with.
/*
class Hello extends Component {
    render(){
        return(
            <div>Hello!</div>
        )
    }
}

class GoodBye extends Component {
    render(){
        return(
            <div>GoodBye!</div>
        )
    }
}

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <BrowserRouter>
            <div>
                    Header
                    <Route path="/hello" component={Hello}/>
                    <Route path="/goodbye" component={GoodBye}/>
            </div>
        </BrowserRouter>
    </Provider>
    , document.querySelector('.container'));
*/